#include "pch.h"
#include "../soa_lib/SmallObject.h"

#include <chrono>
#include <cstdlib>

struct point
{
	char x, y, z;
	point()
	{
		x = 1, y = 2, z = 3;
		__noop();
	}
	//double v, v1, v2, v3, v4, v5, v6, v7, v8, v9, v10;
};

struct pointSmall : SmallObject {
	char x, y, z;
	//double v, v1, v2, v3, v4, v5, v6, v7, v8, v9, v10;
};

TEST(SmallObjectAllocator, Allocate)
{
	pointSmall pt;
	pointSmall *ppt = new pointSmall;
}

TEST(TimePerformance, BulkAllocation) {
	const int n = 1000;
	point* p_array[n];
	pointSmall* ps_array[n];
	for (int i = 0; i < n; ++i)
	{
		p_array[i] = nullptr;
		ps_array[i] = nullptr;
	}

	std::chrono::steady_clock::time_point begin = std::chrono::steady_clock::now();
	for (int v = 0; v < 100000; ++v)
	{
		const int r_pos = v % n;
		if (p_array[r_pos] == nullptr)
		{
			p_array[r_pos] = new point();
		}
		else
		{
			delete p_array[r_pos];
			p_array[r_pos] = nullptr;
		}
	}
	std::chrono::steady_clock::time_point end = std::chrono::steady_clock::now();

	auto std_time = std::chrono::duration_cast<std::chrono::milliseconds>(end - begin).count();

	std::chrono::steady_clock::time_point beginSOA = std::chrono::steady_clock::now();
	for (int t = 0; t < 100000; ++t)
	{
		const int r_pos = t % n;
		if (ps_array[r_pos] == nullptr)
		{
			ps_array[r_pos] = new pointSmall();
		}
		else
		{
			delete ps_array[r_pos];
			ps_array[r_pos] = nullptr;
		}
	}
	std::chrono::steady_clock::time_point endSOA = std::chrono::steady_clock::now();

	auto soa_time = std::chrono::duration_cast<std::chrono::milliseconds>(endSOA - beginSOA).count();
	std::cout << "\033[0;95m" << "[=========>] " << "\033[0;0m";
	std::cout << "\033[0;45m" << "Standard Allocation: " << std_time << " VS Small Object Allocation: " << soa_time << "\033[0;10m" << std::endl;
	ASSERT_LT(soa_time*0.95f, std_time*1.f);

}


TEST(TimePerformance, ButterflyAllocation) {
	const int n = 1000;
	point* p_array[n];
	pointSmall* ps_array[n];
	for(int i = 0; i < n; ++i)
	{
		p_array[i] = nullptr;
		ps_array[i] = nullptr;
	}

	std::chrono::steady_clock::time_point begin = std::chrono::steady_clock::now();
	for(int v = 0; v < 100000; ++v)
	{
		const int r_pos = std::rand() % n;
		if(p_array[r_pos] == nullptr)
		{
			p_array[r_pos] = new point();
		}
		else
		{
			delete p_array[r_pos];
			p_array[r_pos] = nullptr;
		}
	}
	std::chrono::steady_clock::time_point end = std::chrono::steady_clock::now();

	auto std_time = std::chrono::duration_cast<std::chrono::milliseconds>(end - begin).count();

	std::chrono::steady_clock::time_point beginSOA = std::chrono::steady_clock::now();
	for (int t = 0; t < 100000; ++t)
	{
		const int r_pos = std::rand() % n;
		if (ps_array[r_pos] == nullptr)
		{
			ps_array[r_pos] = new pointSmall();
		}
		else
		{
			delete ps_array[r_pos];
			ps_array[r_pos] = nullptr;
		}
	}
	std::chrono::steady_clock::time_point endSOA = std::chrono::steady_clock::now();

	auto soa_time = std::chrono::duration_cast<std::chrono::milliseconds>(endSOA - beginSOA).count();
	std::cout << "\033[0;95m" << "[=========>] " << "\033[0;0m";
	std::cout << "\033[0;45m" << "Standard Allocation: " << std_time << " VS Small Object Allocation: " << soa_time <<"\033[0;10m" <<std::endl;
	ASSERT_LT(soa_time * 0.95f, std_time * 1.f) ;
}