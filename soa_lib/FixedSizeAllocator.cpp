#include "FixedSizeAllocator.h"
#include <cassert>
#define assertm(exp, msg) assert(((void)msg, exp))

void* FixedSizeAllocator::Allocate()
{

		// 1 - Check if the current allocChunk has some space
		if (chunks_.size() > 0 && chunks_[allocChunkPos_].blocksAvailable_ > 0)
		{
			memoryAvailable -= blockSize_;
			return chunks_[allocChunkPos_].Allocate(blockSize_);
		}
		// 2 - Check if there are some chunks with some free space
		for (size_t i = 1; i < chunks_.size(); ++i) {
			const size_t pos_i = (allocChunkPos_ + i) % chunks_.size();
			if (chunks_[pos_i].blocksAvailable_ > 0) {
				allocChunkPos_ = pos_i;
				memoryAvailable -= blockSize_;
				return chunks_[allocChunkPos_].Allocate(blockSize_);
			}
		}
		// 3 - If there is no space free allocate a new chunk
		Chunk newChunk;
		newChunk.Init(blockSize_, numBlocks_);
		assertm((numBlocks_ > 0), "Num blocks is not greater than 0!");
		memoryAvailable += numBlocks_ * blockSize_;
		chunks_.push_back(std::move(newChunk));
		allocChunkPos_ = chunks_.size() - 1;
		deallocChunkPos_ = allocChunkPos_;
		memoryAvailable -= blockSize_;
		return chunks_[allocChunkPos_].Allocate(blockSize_);
}

void FixedSizeAllocator::Deallocate(void* p)
{
	assert(chunks_.size() > 0);
	
	// If p is not inside the current chunk boundaries, find the correct one
	if( (deallocChunkPos_ >= chunks_.size()) || p <   chunks_[deallocChunkPos_].pData_ || p >= (chunks_[deallocChunkPos_].pData_ + blockSize_ * numBlocks_)) {
		// @todo: improve search performances! ...if possible
		for (int i = 0; i < chunks_.size(); ++i) {
			// Check if this boundaries contain p
			if (p >= chunks_[i].pData_ && p < (chunks_[i].pData_ + numBlocks_ * blockSize_)) {
				deallocChunkPos_ = i;
				break;
			}
		}
	}
	// I have found the correct chunk where to perform deallocation
	// Deallocate the chunk and eventually remove it from the vector
	chunks_[deallocChunkPos_].Deallocate(p, blockSize_);
	memoryAvailable += blockSize_;
	if (chunks_[deallocChunkPos_].blocksAvailable_ == numBlocks_ && memoryAvailable > (blockSize_ * numBlocks_)) {
		std::iter_swap(chunks_.begin() + deallocChunkPos_, chunks_.end() - 1);
		chunks_.erase(chunks_.end()-1);
		// If allocChunkPos has been swapped, notify it...
		if(allocChunkPos_ == (chunks_.size()-1))
		{
			allocChunkPos_ = deallocChunkPos_;
		}
		// Avoid to exceed the array boundaries after element elimination
		if(allocChunkPos_ >= chunks_.size())
		{
			--allocChunkPos_;
		}
		deallocChunkPos_ = allocChunkPos_;
	}
}

FixedSizeAllocator::FixedSizeAllocator(unsigned char blockSize, unsigned char numBlocks) : blockSize_{blockSize}, numBlocks_{numBlocks}
{
	allocChunkPos_ = 0;
	deallocChunkPos_ = 0;
}
