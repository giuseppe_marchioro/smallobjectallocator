#pragma once

#include <vector>
#include "Chunk.h"
class FixedSizeAllocator
{
public:
	void* Allocate();
	void Deallocate(void* p);
	FixedSizeAllocator(unsigned char blockSize, unsigned char numBlocks);
private:
	unsigned long memoryAvailable = 0;
	const size_t blockSize_;
	const unsigned char numBlocks_;
	std::vector<Chunk> chunks_;
	size_t allocChunkPos_;
	size_t deallocChunkPos_;
};

