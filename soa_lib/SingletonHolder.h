#pragma once
#include <iostream>
template<class T>
class SingletonHolder {
public:
	static T& getInstance() { 
		static SingletonHolder instance;
		return instance.m_content;
	};
	void operator =(SingletonHolder const&) = delete;
	SingletonHolder(SingletonHolder&) = delete;
private:
	T m_content;
	SingletonHolder() {
	#ifdef _DEBUG_VERBOSE
		printf("Singleton constructor\n");
	#endif
	};
};