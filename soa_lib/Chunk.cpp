#include "Chunk.h"

void Chunk::Init(size_t blockSize, unsigned char blocks)
{
	pData_ = new chunk_t[blocks * blockSize];
	blocksAvailable_ = blocks;
	firstAvailableBlock_ = 0;

	unsigned char i = 0;
	unsigned char* p = pData_;
	for (; i != blocks; p += blockSize) {
		*p = ++i;
	}
}

void* Chunk::Allocate(size_t blockSize)
{
	if(!blocksAvailable_)
		return nullptr;
	chunk_t* pResult = pData_ + (firstAvailableBlock_ * blockSize);
	firstAvailableBlock_ = *pResult;
	--blocksAvailable_;
	return pResult;
}

void Chunk::Deallocate(void* p, size_t blockSize)
{
	*static_cast<chunk_t*>(p) = firstAvailableBlock_;
	firstAvailableBlock_ = (static_cast<chunk_t*>(p) - pData_) / blockSize;
	++blocksAvailable_;
}
