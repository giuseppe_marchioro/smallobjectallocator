#pragma once
#include <vector>

#include "FixedSizeAllocator.h"

class SmallObjectAllocator
{
	const unsigned char m_MaxObjectSize = 32;	//  64bit Grandezza massima di un singolo oggetto
	const unsigned char m_ChunkSize = 0xFF;		// 256bit Grandezza massima di un chunk (dev'essere maggiore di m_MaxObjectSize e suo multiplo?)
public:
	SmallObjectAllocator();
	void* Allocate(size_t numBytes);
	void Deallocate(void* p, size_t size);
private:
	std::vector<FixedSizeAllocator> m_Pool;
};

