#include "SmallObject.h"
#include "SmallObjectAllocator.h"
#include "SingletonHolder.h"
#include <iostream>

void* SmallObject::operator new(size_t size)
{
	return SingletonHolder<SmallObjectAllocator>::getInstance().Allocate(size);
}

void SmallObject::operator delete(void* p, size_t size)
{
	return SingletonHolder<SmallObjectAllocator>::getInstance().Deallocate(p, size);
}

void SmallObject::operator delete[](void* p, size_t size)
{
	return SingletonHolder<SmallObjectAllocator>::getInstance().Deallocate(p, size);
}

SmallObject::~SmallObject()
{
}
