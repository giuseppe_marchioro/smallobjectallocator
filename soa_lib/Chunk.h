#pragma once

struct Chunk
{
	using chunk_t = unsigned char;
	void Init(size_t blockSize, unsigned char blocks);
	void* Allocate(size_t blockSize);
	void Deallocate(void* p, size_t blockSize);
	chunk_t* pData_;
	chunk_t firstAvailableBlock_;
	chunk_t blocksAvailable_;
};

