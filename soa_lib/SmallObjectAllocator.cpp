#include "SmallObjectAllocator.h"

SmallObjectAllocator::SmallObjectAllocator()
{ 
	// We use chunks of m_ChunkSize bits e.g 256
	// And we want to have a collection of fixed size allocators.
	// Each FSA can allocate objects. Since the chunk is fixed
	// Each FSA can allocate a different number of blocks depending on its size
	for (size_t i = 1; i <= m_MaxObjectSize; ++i) {
		unsigned char numBlocks = m_ChunkSize / (i);
		m_Pool.emplace_back(i, numBlocks);
	}
}

void* SmallObjectAllocator::Allocate(size_t numBytes)
{
	if (numBytes > m_MaxObjectSize) {
		return malloc(numBytes);
	}
	void* memoryAddress = m_Pool[numBytes - 1].Allocate();
	return  memoryAddress;
}

void SmallObjectAllocator::Deallocate(void* p, size_t size)
{
	if (size > m_MaxObjectSize) {
		return free(p);
	}
	return  m_Pool[size - 1].Deallocate(p);
}
